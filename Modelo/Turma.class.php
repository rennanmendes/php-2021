<?php
class Turma{
	private $id;
	private $ano;
	private $nivel;
	private $serie;
	private $turno;
	
	

    private function __construct(){

    }
    private function __destruct(){

    }
    public function getAno(){
		return $this->ano;
	}
	public function setAno($n){
		$this->ano = (isset($n)) ? $n : NULL;
	}
	public function getNivel(){
		return $this->nivel;
	}
	public function setNivel($i){
		$this->nivel= (isset($i)) ? $i : NULL;
	}
	public function getSerie(){
		return $this->serie;
	}
	public function setSerie($c){
		$this->serie= (isset($c)) ? $c : NULL;
	}
	public function getTurno(){
		return $this->turno;
	}
	public function setTurno($c){
		$this->turno= (isset($c)) ? $c : NULL;
	}
	public function getId(){
		return $this->id;
	}
	public function setId($c){
		$this->id= (isset($c)) ? $c : NULL;
	}
	
}
?>