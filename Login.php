<?php
	echo "
		<!DOCTYPE html>
		<html lang='pt-br'>
		<head>
		<title>LastSummer.com</title>
		<meta charset='UTF-8'>
		<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>
		<link type='text/css' rel='stylesheet' href='css/materialize.min.css' media='screen,projection'/>
		<link type='text/css' rel='stylesheet' href='css/estilo.css'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
		</head>
		<body>

		<center>
			<h4>Faça o login e tenha acesso a outros recursos</h4>
			<center>
		 	<form action='processaAluno.php' method='post'>
           
            <label>Login:</label>
            <input type='text' name='login' id='l' /><br /> <br />
            <label>Senha:</label>
            <input type='password' name='senha' id='s' /><br /> <br />
            <input type='submit' class='waves-effect  btn-small grey darken-3' name='enviar' value='Enviar'  /></br><br/>
            <a class='waves-effect waves-light btn-small grey darken-3'  href='pesquisaAluno.php'>Pesquisar suas informações</a>
            <a type='submit'  class='enviar waves-effect  btn-small grey darken-3' href='atualizarAluno.php'>Atualizar suas informações</a>
            <a type='submit'  class='enviar waves-effect  btn-small grey darken-3' href='apagarAluno.php'>Cancelar conta</a>
        </form>
        <center/>
        </br>
        </br>
			<a class='waves-effect waves-light btn-small grey darken-3' href='cadastro.php'>Cadastre-se</a>
			</form>
		<center/>
		</br>
		</br>

		 <footer class='page-footer grey darken-3'> 
		 	<div class='container'> 
		 	<div class='row'> 
		 	<div class='col l6 s12'> 
		 	<h5 class='white-text'>Obrigado pela visita.</h5> 
		 	<p class='grey-text text-lighten-4'>Esse é um pequeno sistema usado como prova na disciplina de php.</p> 
		 	</div> 
		 	<div class='col l4 offset-l2 s12'> 
		 	<h5 class='white-text'>Bibliografias</h5> 
		 	<ul> 
		 		<li><a class='grey-text text-lighten-3' href='https://pt.wikipedia.org/wiki/Shawn_Mendes'>https://pt.wikipedia.org/wiki/Shawn_Mendes</a></li> 
		 		<li><a class='grey-text text-lighten-3' href='https://pt.wikipedia.org/wiki/Camila_Cabello'>https://pt.wikipedia.org/wiki/Camila_Cabello</a></li> 
		 		 
		 	</ul> 
		 	</div> 
		 	</div> 
		 	</div> 
		 	<div class='footer-copyright'> 
		 	<div class='container'> © 2014 Copyright Text 
		 	<a class='grey-text text-lighten-4 right' href='#'>Criado por @renanhmendes.</a> 
		 	</div> 
		 	</div> 
		</footer>
		
		<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
		<script type='text/javascript' src='js/materialize.min.js'></script>
		</body>
		</html>




	";
	require_once("controle/controleAluno.class.php");
        session_start();
        if(isset($_SESSION['erro'])){
            echo "<script>alert(\"{$_SESSION['erro']}\")</script>";
            session_destroy();
        }
        require_once("mvc/controle/ControleAluno.class.php");
        $comando = new ControleAluno();
    
?>