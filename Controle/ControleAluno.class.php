<?php
//verifica requiquisição
require_once("Conection.class.php");
require_once("Aluno.class.php");
final class FanControle{
	public function consultaTodos(){
		$Conection = new Conection ("mysql.ini");
		$comando = $Conection->getConection()->prepare("SELECT * FROM aluno;");
		$comando->execute();
		//pegar as informaçoes da tabela.
		$resutado = $comando->fetchAll();
		$lista=[];
		foreach($resutado as $item){//ligação de dados da classe com o banco de dados
			// criando objetos da classe aluno
			$usuario = new Aluno();
			$usuario->id($item->id);
			$usuario->setNome($item->nome);
			$usuario->setTelefone($item->telefone);
			$usuario->setEmail($item->email);
			$usuario->setData($item->data);
			$usuario->setGenero($item->genero);
		return $lista;
		$comando->__destruct();

			
		}
	}
	public function adicionarAluno($usuario){
		$Conection = new Conection ("mysql.ini");
		$sql = "INSERT INTO aluno(nome,data,telefone,email,genero) VALUES(:nome,:data,:telefone,:email,:genero)";
		$comando = $Conection->getConection()->prepare($sql);
		$comando->bindParam("nome", $usuario->getNome());
		$comando->bindParam("telefone", $usuario->getTelefone());
		$comando->bindParam("data", $usuario->getData());
		$comando->bindParam("email", $usuario->getEmail());
		$comando->bindParam("genero", $usuario->getGenero());
		
		if($comando->execute()){
			$Conection->__destruct();
			return true;

		}else{
			$Conection->__destruct();
			return false;
		}
	}
	public function atualizarArtista($fan){
		$Conection=new Conection("mysql.ini");
		$sql="UPDATE aluno SET nome=:nome, telefone=:telefone, email=:email, data=:data, genero=:genero WHERE id=:id; ";
		$comando = $Conection->getConection()->prepare($sql);
		$comando->bindParam("nome", $usuario->getNome());
		$comando->bindParam("telefone", $usuario->getTelefone());
		$comando->bindParam("data", $usuario->getData());
		$comando->bindParam("email", $usuario->getEmail());
		$comando->bindParam("genero", $usuario->getGenero());
		$comando->execute();
		$comando->__destruct();

	}																
	public function removerArtista($id){
		$Conection = new Conection ("mysql.ini");
		// Vai apagar a linha e mantar o restante
		$comando = $Conection->getConection()->prepare("DELETE FROM aluno WHERE id=:id");
		$comando->bindParam("id", $id);
		$comando->execute();
		//retorno booleano
		$Conection->_destruct();
		
	}
}

?>